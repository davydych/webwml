#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20" maintainer="Erik Pfannenstein"
<define-tag pagetitle>DebConf20 Online schließt</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
Am Samstag, dem 29. August 2020 ging die jährliche Debian-Entwickler und 
-Unterstützerkonferenz zu Ende.</p>

<p>
Die DebConf 20 war wegen der Coronavirus-Pandemie (COVID-19) die erste 
DebConf, die im Internet stattfand.
</p>

<p>
Alle Sitzungen wurden geströmt und boten mehrere Möglichkeiten zur Teilnahme 
via IRC-Nachrichten, gemeinsam bearbeitete Online-Textdateien und 
Videokonferenzräume.
</p>

<p>
Mit über 850 Teilnehmerinnen und Teilnehmern aus 80 verschiedenen Ländern 
sowie insgesamt 100 Vorträgen, Diskussionsrunden, 
»Birds of a Feather«-Zusammenkünften (BoF) und anderen Veranstaltungen war die 
<a href="https://debconf20.debconf.org">DebConf20</a> ein großer Erfolg.
</p>

<p>
Als deutlich wurde, dass die DebConf20 eine reine Online-Veranstaltung sein 
würde, verbrachte das DebConf-Video-Team in den folgenden Monaten viel Zeit 
mit dem Adaptierung, Verbesserung und in einigen Fällen sogar der kompletten 
Neukonzeption der Technik, die nötig sein würde, um diese Form der 
DebConf zu ermöglichen. Anhand von Erfahrungen der MiniDebConfOnline Ende 
März wurden ein paar Anpassungen vorgenommen und diese führten am Ende zu 
einer Lösung bestehend aus Jitsi, OBS, Voctomix, SReview, nginx, Etherpad und 
einem neu geschriebenen webbasierten Frontend für Voctomix.
</p>


<p>
Alle Komponenten der Video-Infrastruktur sind freie Software und werden durch 
ihr öffentliches 
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>-Depot 
konfiguriert.
</p>

<p>
Nicht alle Vortrags-Tracks waren auf Englisch: Auf dem <a href="https://debconf20.debconf.org/schedule/">Veranstaltungskalender</a> standen auch die spanische MiniConf mit ihren acht 
Vorträgen in zwei Tagen sowie die Malayalam-MiniConf, auf der in drei Tagen 
neun Vorträge gehalten wurden.
Wie immer gab es im Laufe der Konferenz von Seiten der Anwesenden mehrere 
Ad-hoc-Aktivitäten, die ebenfalls ausgestrahlt und aufgezeichnet worden sind. 
Die Debian-Teams kamen zu mehreren 
<a href="https://wiki.debian.org/Sprints/">Sprints</a> zusammen, um die 
Entwicklung in bestimmten Bereichen voranzutreiben.
</p>

<p>
Zwischen den Vorträgen zeigte der Videostream die üblichen Sponsoren 
in Dauerschleife. Außerdem waren Fotos der vorherigen DebConfs, witzige 
Fakten über Debian und kurze Grußvideos von Teilnehmerinnen und Teilnehmern 
an ihre Bekannten zu sehen.</p>

<p>
Für diejenigen, die nicht teilnehmen konnten, sind die meisten Vorträge und 
Sitzungen bereits auf der <a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">Debian-Treffen-Archiv-Website</a> eingestellt worden. Der Rest 
wird in den kommenden Tagen nachfolgen.
</p>

<p>
Die <a href="https://debconf20.debconf.org/">DebConf20</a>-Website wird zu 
Archivzwecken verfügbar bleiben und Links zu den Präsentationen und Videos 
der Vorträge bzw. Veranstaltungen anbieten.
</p>


<p>
Es wird geplant, <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> nächstes 
Jahr im August oder September in Haifa, Israel stattfinden zu lassen.
</p>

<p>
Die DebConf möchte allen Teilnehmerinnen und Teilnehmern eine sichere und 
einladende Umgebung bieten. Während der Konferenz waren mehrere Teams (Front 
Desk, Willkommens- und Gemeinschafts-Team) beratend zur Stelle, um allen 
Anwesenden einen angenehmen Aufenthalt zu ermöglichen und Lösungen für allerlei 
auftretende Probleme zu finden.
Siehe auch die  <a href="https://debconf20.debconf.org/about/coc/">DebConf20-Website mit dem Verhaltenskodex</a> für weitere Details.
</p>


<p>
Debian bedankt sich für das Engagement zahlreicher 
<a href="https://debconf20.debconf.org/sponsors/">Sponsoren</a>, 
durch das sie die DebConf20 ermöglicht haben; vor allem danken wir unseren 
Platinum-Sponsoren:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
und 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>Über Debian</h2>

<p>Das Debian-Projekt wurde 1993 von Ian Murdock als wirklich freies 
Gemeinschaftsprojekt gegründet. Seitdem ist das Projekt zu einem der größten 
und einflussreichsten Open-Source-Projekte angewachsen. Tausende von 
Freiwilligen aus aller Welt arbeiten zusammen, um Debian-Software herzustellen 
und zu betreuen. Verfügbar in über 70 Sprachen und eine große Bandbreite an 
Rechnertypen unterstützend bezeichnet sich Debian als das <q>universelle 
Betriebssystem</q>.</p>

<h2>Über die DebConf</h2>

<p>
Die DebConf ist die Entwicklerkonferenz des Debian-Projekts. Neben einem 
mit technischen, sozialen und politischen Vorträgen gefüllten Zeitplan bietet 
die DebConf eine Möglichkeit für Entwicklerinnen, Unterstützer und andere 
Interessierte, sich persönlich zu treffen und enger zusammenzuarbeiten. Sie 
findet seit 2000 jedes Jahr an verschiedenen Orten wie Schottland, Argentinien 
und Bosnien-Herzegowina statt. Weitere Informationen zur DebConf sind unter 
<a href="https://debconf.org/">https://debconf.org</a> verfügbar.
</p>

<h2>Über Lenovo</h2>

<p>
Als globaler Technikanführer, der ein breites Portfolio von 
vernetzten Produkten wie Smartphones, Tablets, PCs und Workstations sowie  
AR-/VR-Geräte, Smart Home/Office und Rechenzentrumslösungen herstellt, ist sich  
<a href="https://www.lenovo.com">Lenovo</a> bewusst,  
wie kritisch offene Systeme und Plattformen für eine vernetzte Welt sind.
</p>


<h2>Über Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> ist das größte 
Webhosting-Unternehmen der Schweiz. Es bietet unter anderem Backup- und 
Speicherdienste sowie Lösungen für Veranstaltungen, Live-Streaming und 
Video-on-Demand-Dienste an. Alle seine Rechenzentren und sämtliche 
Bestandteile, deren Funktionieren für die Dienste und Produkte (sowohl Hard- 
als auch Software) unerlässlich sind, sind vollständig im Besitz der Informaniak.
</p>

<h2>Über Google</h2>

<p>
<a href="https://google.com/">Google</a> ist eines der größten Technik- 
Unternehmen der Welt; sein Angebot umfasst sehr viele verschiedene 
Internet-bezogene Dienste und Produkte wie Onlinewerbung, Suche, Cloud- 
Computing, Software und Hardware.
</p>

<p>
Google unterstützt Debian schon seit mehr als zehn Jahren als 
DebConf-Sponsor und betreibt darüber hinaus als Debian-Partner Teile der 
 <a href="https://salsa.debian.org">Salsa</a>-Continuous-Integration-Infrastruktur auf der Google-Cloud-Plattform.
</p>

<h2>Über Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> ist eine der 
umfangreichsten und am häufigsten eingesetzten Cloud-Plattformen. Sein Angebot 
umfasst mehr als 175 Komplettdienste von global verteilten Rechenzentren 
(in 77 Verfügbarkeitszonen, die sich in 24 geografischen Regionen befinden). 
Zu den AWS-Kunden gehören sowohl die am schnellsten wachsenden Startups als 
auch die größten Unternehmen und führenden Regierungsorganisationen.
</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen sie bitte die DebConf20-Website unter 
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
oder senden eine E-Mail (auf Englisch) an &lt;press@debian.org&gt;.</p>
