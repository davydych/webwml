#use wml::debian::translation-check translation="94f25ae9cef2ff960a0dd2a976d6022f2789bb9f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19318">CVE-2019-19318</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19813">CVE-2019-19813</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

<p><q>Team bobfuzzer</q> a signalé des bogues dans Btrfs qui pourraient conduire
à une utilisation de mémoire après libération ou à un dépassement de tampon de
tas pouvant être déclenchés par des images contrefaites de système de fichiers.
Un utilisateur autorisé à monter et accéder à des systèmes de fichiers arbitraires
pourrait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

<p>Un défaut a été signalé dans le code du système de fichiers JFS
permettant à un attaquant local avec la capacité de configurer des
attributs étendus de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

<p>Adam <q>pi3</q> Zabrocki a signalé un défaut d'utilisation de mémoire
après libération dans la logique de redimensionnement du tampon circulaire
de ftrace à cause d'une situation de compétition, qui pourrait avoir pour
conséquence un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

<p>David Disseldorp a découvert que l'implémentation de la cible SCSI LIO
réalisait une vérification insuffisante dans certaines requêtes XCOPY. Un
attaquant avec accès à un Logical Unit Number et la connaissance des
affectations d'Unit Serial Number peut tirer avantage de ce défaut pour
lire et écrire sur n'importe quel « backstore » de LIO, indépendamment des
réglages de transport de SCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568</a> (<a href="https://xenbits.xen.org/xsa/advisory-349.html">XSA-349</a>)

<p>Michael Kurth et Pawel Wieczorkiewicz ont signalé que les interfaces
peuvent déclencher un débordement de mémoire dans les dorsaux en mettant à jour
un chemin surveillé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569</a> (<a href="https://xenbits.xen.org/xsa/advisory-350.html">XSA-350</a>)

<p>Olivier Benjamin et Pawel Wieczorkiewicz ont signalé un défaut
d'utilisation de mémoire après libération qui peut être déclenché par une
interface bloc dans blkback de Linux. Un client qui se comporte mal peut
déclencher un plantage de dom0 en se connectant et se déconnectant
continuellement à une interface bloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

<p>Jann Horn a signalé un problème d'incompatibilité de verrouillage dans le
sous-système tty qui peut permettre à un attaquant local de monter une
attaque de lecture après libération à l'encontre de TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

<p>Jann Horn a signalé un problème de verrouillage dans le sous-système tty
qui peut provoquer une utilisation de mémoire après libération. Un
attaquant local peut tirer avantage de ce défaut pour une corruption de
mémoire ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

<p>Un défaut de dépassement de tampon a été découvert dans le pilote WiFi
mwifiex qui pourrait provoquer un déni de service ou l'exécution de code
arbitraire à l'aide d'une longue valeur de SSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3178">CVE-2021-3178</a>

<p>吴异 a signalé une fuite d'informations dans le serveur NFSv3. Lorsqu’un
seul sous-répertoire d’un volume de système de fichiers est exporté, un client
NFS à l’écoute du répertoire exporté obtiendrait un gestionnaire de fichier
pour un répertoire parent, permettant un accès à des fichiers n’étant pas
susceptibles d’être exportés.</p>

<p>Même après cette mise à jour, il est encore possible à des clients NFSv3
de deviner des gestionnaires de fichier valables et d'accéder à des fichiers en
dehors du sous-répertoire exporté, à moins que l’option d’exportation
<q>subtree_check</q> soit activée. Il est recommandé de ne pas utiliser cette
option, mais de seulement exporter des volumes entiers de système de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

<p>Les futex de PI sont sujets à une utilisation de mémoire après
libération de la pile du noyau durant la gestion d'erreur. Un
utilisateur non privilégié pourrait utiliser ce défaut pour le plantage du
noyau (provoquant un déni de service) ou pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a> (<a href="https://xenbits.xen.org/xsa/advisory-365.html">XSA-365</a>)

<p>Olivier Benjamin, Norbert Manthey, Martin Mazein et Jan H. Schönherr ont
découvert que le pilote de dorsal de bloc Xen (xen-blkback) ne gérait pas
correctement les erreurs de mappage d’allocation. Un invité malveillant pourrait
exploiter ce bogue pour provoquer un déni de service (plantage), ou éventuellement
une fuite d'informations ou une élévation des privilèges, dans le domaine
exécutant le dorsal, qui est classiquement dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a> (<a href="https://xenbits.xen.org/xsa/advisory-362.html">XSA-362</a>), <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> (<a href="https://xenbits.xen.org/xsa/advisory-361.html">XSA-361</a>), <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> (<a href="https://xenbits.xen.org/xsa/advisory-367.html">XSA-367</a>)

<p>Jan Beulich a découvert que le code de prise en charge de Xen et de divers
pilotes de dorsal Xen ne gérait pas correctement les erreurs de mappage
d’allocation. Un invité malveillant pourrait exploiter ce bogue pour provoquer
un déni de service (plantage), ou éventuellement une fuite d'informations ou une
élévation des privilèges, dans le domaine exécutant le dorsal, qui est
classiquement dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement l’accès aux attributs de gestion de transport dans sysfs. Dans un
système agissant comme initiateur iSCSI, cela constitue une fuite d'informations
vers des utilisateurs locaux et facilite l’exploitation du <a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement l’accès à son interface de gestion netlink. Dans un système
agissant comme initiateur iSCSI, un utilisateur local pourrait utiliser cela
pour provoquer un déni de service (déconnexion du stockage) ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement la longueur des paramètres ou des <q>passthrough PDU</q> envoyés
à travers son interface de gestion netlink. Dans un système agissant comme
initiateur iSCSI, un utilisateur local pourrait utiliser cela pour faire fuiter
le contenu de la mémoire du noyau, pour provoquer un déni de service (corruption
de mémoire du noyau ou plantage) et probablement pour une élévation des
privilèges.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9.258-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2586.data"
# $Id: $
