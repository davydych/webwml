#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il y avait deux vulnérabilités de déni de service dans graphicsmagick,
une collection d'outils de traitement d'images :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5240">CVE-2016-5240</a>

<p>Déni de service évité en détectant et en rejetant des arguments
« stroke-dasharray » négatifs qui provoquaient une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5241">CVE-2016-5241</a>

<p>Correction d'un problème de division par zéro si une image de motif de
remplissage ou de traits ne comportait aucune colonne ou ligne, pour éviter
une attaque par déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 1.3.16-1.1+deb7u3 de graphicsmagick.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-574.data"
# $Id: $
