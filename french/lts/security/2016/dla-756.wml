#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>De nombreuses vulnérabilités ont été découvertes dans ImageMagick, un
programme de manipulation d'image. Les problèmes comprennent des exceptions
de mémoire, des dépassements de tas, de tampon et de pile, des lectures
hors limites et des vérifications manquantes.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 8:6.7.7.10-5+deb7u10.</p>

<p>L'impact exact des vulnérabilités est inconnu, dans la mesure où elles
ont été pour la plupart découvertes par des tests à données aléatoires.
Nous vous recommandons encore de mettre à niveau vos paquets d'imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-756.data"
# $Id: $
