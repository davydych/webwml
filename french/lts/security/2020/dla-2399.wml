#use wml::debian::translation-check translation="a5930e9a6b786095b420236a41293d34f49875c3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans packagekit, un service de gestion de
paquets.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16121">CVE-2020-16121</a>

<p>Vaisha Bernard a découvert que PackageKit gérait incorrectement certaines
méthodes. Un attaquant local pourrait utiliser cela pour connaître le type MIME
de n’importe quel fichier sur le système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16122">CVE-2020-16122</a>

<p>Sami Niemimäki a découvert que PackageKit gérait incorrectement certains
paquets deb locaux. Un utilisateur local pourrait éventuellement utiliser cela
pour installer des paquets non autorisés.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.1.5-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets paquetskit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité paquets dekit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/packagekit">https://security-tracker.debian.org/tracker/packagekit</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2399.data"
# $Id: $
