#use wml::debian::translation-check translation="d3f29d8015c29a9da9f70c50fcc3cb13a49a95c7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans cURL, une bibliothèque
de transfert par URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5436">CVE-2019-5436</a>

<p>Un dépassement de tampon de tas a été découvert dans le code de réception
de TFTP qui pourrait permettre un déni de service ou l'exécution de code
arbitraire. Cela n'affecte que la distribution oldstable (Stretch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5481">CVE-2019-5481</a>

<p>Thomas Vegas a découvert une double libération de zone de mémoire dans
le code de FTP-KRB, déclenchée par l'envoi par un serveur malveillant de
blocs de données de très grande taille.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

<p>Thomas Vegas a découvert un dépassement de tas qui pourrait être
déclenché lors de l'utilisation de petites tailles de bloc TFTP différentes
de celles par défaut.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 7.52.1-5+deb9u10.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 7.64.0-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4633.data"
# $Id: $
