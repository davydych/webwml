#use wml::debian::translation-check translation="2b74abc752f8e4232fe85da5b7c01782113a2f4d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, som kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang rapporterede om en kapløbstilstand i libsas, kerneundersystemet 
    som understøtter Serial Attached SCSI-enheder (SAS), hvilket kunne føre til 
    en anvendelse efter frigivelse.  Det står ikke klart hvordan det kan 
    udnyttes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>Man opdagede at de fleste x86-processorer kunne spekulativt springe over 
    en betinget SWAPGS-instruktion, som anvendes når man går ind i kernen fra 
    brugertilstand, og/eller kunne spekulativt udføre den, når den skulle 
    springes over.  Det er en undertype af Spectre variant 1, som kunne gøre det 
    muligt for lokale brugere at få adgang til følsomme oplysninger fra kernen 
    eller andre processer.  Det er løst ved at anvende hukommelsesbarrierer til 
    at begrænse spekulativ udførelse.  Systemer, der anvender en i386-kerne, er 
    ikke påvirket, da kernen ikke anvender SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1999">CVE-2019-1999</a>

    <p>En kapløbstilstand blev opdaget i Android-binderdriveren, hvilken kunne 
    føre til en anvendelse efter frigivelse.  Hvis denne driver er indlæst, 
    kunne en lokal bruger måske være i stand til at anvende fejlen til et 
    lammelsesangreb (hukommelseskorruption) eller til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>Værktøjet syzkaller fandt en potentiel nulldereference i forskellige 
    drivere til UART-tilsluttede Bluetooth-adaptere.  En lokal bruger med 
    adgang til en pty-enhed eller andre passende tty-enheder, kunne anvende 
    fejlen til et lammelsesangreb (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein og Benny Pinkas opdagede at genereringen af IP-pakke-id'er 
    anvendte en svag hash-funktion, <q>jhash</q>.  Det kunne medføre sporing af 
    individuelle computere, når de kommunikerer med forskellige fjerne servere 
    og fra forskellige netværk.  I stedet anvendes nu funktionen 
    <q>siphash</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12817">CVE-2019-12817</a>

    <p>Man opdagede at på PowerPC-arkitekturen (ppc64el), håndterede hash page 
    table-koden (HPT) ikke på korrekt vis fork() i en proces med hukommelses 
    mappet til adresser over 512 TiB.  Det kunne føre til anvendelse efter 
    frigivelse i kernen, eller utilsigtet deling af hukommelse mellem 
    brugerprocesser.  En lokal bruger kunne anvende fejlen til 
    rettighedsforøgelse.  Systemer, der anvender radix MMU'en, eller en 
    skræddersyet kerne med en sidestørrelse på 4 KiB, er ikke 
    påvirkede.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12984">CVE-2019-12984</a>

    <p>Man opdagede at implementeringen af NFC-protokollen ikke på korrekt vis 
    validerede en netlink-kontrolmeddelelse, potentielt førende til en 
    nullpointerdereference.  En lokal bruger med en NFC-grænseflade, kunne 
    udnytte fejlen til lammelsesangreb (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13233">CVE-2019-13233</a>

    <p>Jann Horn opdagede en kapløbstilstand på x86-arkitekturen, ved anvendelse 
    af LDT'en.  Det kunne føre til anvendelse efter frigivelse.  En lokal bruger 
    kunne måske anvende fejlen til lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>Man opdagede at gtco-driveren til USB-inputtablets kunne overløbe en 
    stakbuffer med konstante data, mens en enheds descriptor blev fortolket.  En 
    fysisk tilstedeværende bruger med en særligt fremstillet USB-enhed, kunne 
    udnytte fejlen til at forårsage et lammelsesangreb (BUG/oops) eller måske 
    til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey rapporterede at på PowerPC-systemer (ppc64el) uden 
    Transactional Memory (TM), forsøgte kernen alligevel at gendanne TM-state 
    overført til systemkaldet sigreturn() system call.  En lokal bruger kunne 
    udnytte fejlen til lammelsesangreb (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>Værktøjet syzkaller fandt en manglende grænsekontrol i diskettedriveren.  
    En lokal bruger med adgang til en disketteenhed, uden en diskette, kunne 
    anvende fejlen til at læse kernehukommelse ud over I/O-bufferen, og måske 
    få fat i følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>Værktøjet syzkaller fandt en potentiel division med nul i 
    diskettedriveren.  En lokal bruger med adgang til en disketteenhed, kunne 
    anvende fejlen til lammelsesangreb (oops).</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.37-5+deb10u2.</p>

<p>I den gamle stabile distribution (stretch), vil disse problemer snart blive 
rettet.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4495.data"
