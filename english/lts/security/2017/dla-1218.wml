<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in rsync, a fast, versatile,
remote (and local) file-copying tool, allowing a remote attacker to
bypass intended access restrictions or cause a denial of service.</p>


<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.9-4+deb7u1.</p>

<p>We recommend that you upgrade your rsync packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1218.data"
# $Id: $
