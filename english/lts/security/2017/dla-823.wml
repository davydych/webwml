<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that a programming error in the processing of HTTPS
requests in the Apache Tomcat servlet and JSP engine may result in
denial of service via an infinite loop.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u10.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-823.data"
# $Id: $
