<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Lyon Yang discovered that the C client shells cli_st and cli_mt of
Apache Zookeeper, a high-performance coordination service for
distributed applications, were affected by a buffer overflow
vulnerability associated with parsing of the input command when using
the "cmd:" batch mode syntax. If the command string exceeds 1024
characters a buffer overflow will occur.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.3.5+dfsg1-2+deb7u1.</p>

<p>We recommend that you upgrade your zookeeper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-630.data"
# $Id: $
