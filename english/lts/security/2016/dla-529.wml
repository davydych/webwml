<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A denial of service vulnerability was identified in Commons FileUpload
that occurred when the length of the multipart boundary was just below
the size of the buffer (4096 bytes) used to read the uploaded file.
This caused the file upload process to take several orders of
magnitude longer than if the boundary was the typical tens of bytes long.</p>

<p>Apache Tomcat uses a package renamed copy of Apache Commons FileUpload
to implement the file upload requirements of the Servlet specification
and was therefore also vulnerable to the denial of service vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u5.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-529.data"
# $Id: $
