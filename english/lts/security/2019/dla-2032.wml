<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was unsafe deserialisation issue in cacti,
server monitoring system system. Unsafe deserialisation of objects which can
lead to abuse of the application logic, deny service or even execute arbitrary
code.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a></li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.8b+dfsg-8+deb8u8.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2032.data"
# $Id: $
