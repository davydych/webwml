<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Denis Andzakovic discovered that network-manager-vpnc, a plugin to
provide VPNC support for NetworkManager, is prone to a privilege
escalation vulnerability. A newline character can be used to inject a
Password helper parameter into the configuration data passed to vpnc,
allowing a local user with privileges to modify a system connection to
execute arbitrary commands as root.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9.10.0-1+deb8u1.</p>

<p>We recommend that you upgrade your network-manager-vpnc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1454.data"
# $Id: $
